package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class Hustlercheck extends TestBaseTG {
	
	final WebDriver driver;
	public Hustlercheck(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInHustler() {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Hustler - landing");
		
		

	Select seleccionDePais = new Select(driver.findElement(By.name("country-select")));
		seleccionDePais.selectByVisibleText("Salvador");
		espera(3000);
	
	Select seleccionDeOperador = new Select(driver.findElement(By.name("operator-select")));
		seleccionDeOperador.selectByVisibleText("Digicel");
		espera(3000);
		
			IngresaLineaTelefonica.sendKeys("11111189");
		
		checkMayorDeEdad.click();
			espera(1000);
			
			btnContinue.click();
			
			espera(3000);
					
	}		

	public void verifySections(String apuntaA, String ambiente) {

		//Seccion de Videos
		
		driver.get(apuntaA + "xxx.tulandia.net/category/videos/blonde/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/category/videos/brunette/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/category/videos/redhead/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/category/videos/latina/"); //Seccion sin contenido
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/category/videos/asian/"); //Seccion sin contenido
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/category/videos/");
			espera(500);
			
		//Seccion de Fotos	
		driver.get(apuntaA + "xxx.tulandia.net/galleries/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/julia-ann-2/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/brooklyn-lee/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/jayden-jaymes-2/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/faye-reagan/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/aletta-ocean-2/");
			espera(500);
		driver.get(apuntaA + "http://xxx.tulandia.net/aletta-ocean/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/alison-tyler/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/tori-black/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/evilyn-fierce/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/jayden-jaymes/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/aidra-fox/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/julia-ann/");
			espera(500);
		driver.get(apuntaA + "xxx.tulandia.net/emma-mae/");
			espera(500);
		
		
		espera(3000);
		
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Hustler"); 
		System.out.println();
		System.out.println("Fin de Test Hustler - Landing");
		
	}
}  

